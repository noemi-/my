//
//  MYMakePostingViewController.h
//  MissingYou
//
//  Created by Maira on 11/9/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>


@interface MYMakePostingViewController : UIViewController <UITextFieldDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate>
{
    UIImage *chosenPicture;
}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *menuButton;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UITextField *personNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *personAgeTextField;
@property (weak, nonatomic) IBOutlet UISegmentedControl *genderSwitch;
@property (weak, nonatomic) IBOutlet UITextField *locationFoundTextField;
@property (weak, nonatomic) IBOutlet UITextField *contactNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *addressTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) UITextField * activeTextField;
-(void)takePicture;
- (IBAction)picturePressed:(id)sender;
-(void)uploadPicture;
@end
