//
//  MYPerson.h
//  MissingYou
//
//  Created by Noemi Quezada on 11/6/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MYPostItemProtocol.h"
#import  <Parse/Parse.h>

@interface MYPerson : NSObject <MYPostItemProtocol, NSObject>


@property (strong, nonatomic)NSString * firstName;
@property (strong, nonatomic)NSString * lastName;
@property (strong, nonatomic)NSString * middleName;
@property (strong, nonatomic)NSString * hairColor;
@property (strong, nonatomic)NSString * eyeColor;
@property (assign, nonatomic)double height;
@property (assign, nonatomic)double weight;
@property (assign, nonatomic)NSString * gender;
@property (strong, nonatomic)PFFile * personPicture;
@property (assign, nonatomic)BOOL found;
@property (assign, nonatomic)int  age;


/* Init method */
-(id)initWithPersonId: (NSString *)aPersonId aFirstName: (NSString *)afirstName aLastName: (NSString *)aLastName aMiddleName:(NSString *)aMiddleName aHairColor:(NSString *)aHairColor anEyeColor: (NSString *)anEyeColor aHeight: (double)aHeight aWeight: (double)aWeight aGender:(NSString *)aGender anAge: (double)anAge aProfilePicture: (PFFile *)aPicture;


@end
