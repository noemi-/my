//
//  MYResultsTableViewController.h
//  MissingYou
//
//  Created by Maira on 11/8/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "MYPerson.h"

@interface MYResultsTableViewController : PFQueryTableViewController <UISearchBarDelegate>
{
    NSMutableArray *people;
    NSMutableArray *searchResults;

}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarbutton;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;



@end
