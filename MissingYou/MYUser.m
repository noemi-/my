//
//  MYUser.m
//  MissingYou
//
//  Created by Noemi Quezada on 11/8/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import "MYUser.h"


static MYUser *_sharedMYUserInstance;
@implementation MYUser

@synthesize userId;
@synthesize name;
@synthesize email;


-(MYUser *) init
{
    self = [super init];
    if (self)
    {
        userId = @"";
        name = @"";
        email = @"";
    }
    
    return self;
}

-(id)initWithUserId:(NSString *)aUserId aName:(NSString *)aName anEmail:(NSString *)anEmail
{
    self = [super init];
    if (self)
    {
        userId = aUserId;
        name = aName;
        email = anEmail;
    }
    
    return self;
}

+(void)initialize
{
    if ([MYUser class] == self)
    {
        _sharedMYUserInstance = [self new];
    }
    
}

+(MYUser *) sharedMYUserInstance
{
    return _sharedMYUserInstance;
}


+(id) allocWithZone:(NSZone *)zone
{
    if (_sharedMYUserInstance && [MYUser class] == self)
    {
        [NSException raise:NSGenericException format:@"May not create more than one instance of a singleton class!"];
    }
    return [super allocWithZone:zone];
}

@end
