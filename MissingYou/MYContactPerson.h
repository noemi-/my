//
//  MYContactPerson.h
//  MissingYou
//
//  Created by Noemi Quezada on 11/8/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MYAddress.h"

@interface MYContactPerson : NSObject

@property (strong, nonatomic)NSString * contactPersonId;
@property (strong, nonatomic)NSString * name;
@property (strong, nonatomic)NSString * phoneNumber;
@property (strong, nonatomic)NSString * email;
@property (strong, nonatomic)MYAddress * address; 

/* Init method */
-(id)initWithContactPersonId: (NSString *)aContactPersonId aName: (NSString *)aName aPhoneNumber: (NSString *)aPhoneNumber anEmail:(NSString *)anEmail anAddress: (MYAddress *)anAddress;


@end
