//
//  MYUser.h
//  MissingYou
//
//  Created by Noemi Quezada on 11/8/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MYUser : NSObject

@property (strong, nonatomic) NSString * userId;
@property (strong, nonatomic) NSString * name;
@property (strong, nonatomic) NSString * email;

/* Init method */
-(id)initWithUserId: (NSString *)aUserId aName: (NSString *)aName anEmail:(NSString *)anEmail;



@end
