//
//  MYLocation.m
//  MissingYou
//
//  Created by Noemi Quezada on 11/8/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import "MYLocation.h"

@implementation MYLocation

@synthesize locationId;
@synthesize cityTown;
@synthesize country;
@synthesize governingDistrict;
@synthesize postalArea;

-(id)initWithLocationId:(NSString *)aLocationId aCountry:(NSString *)aCountry aGoverningDistrict:(NSString *)aGoverningDistrict aPostalArea:(NSString *)aPostalArea aCityTown:(NSString *)aCityTown
{
    self = [super init];
    if (self)
    {
        locationId = aLocationId;
        cityTown = aCityTown;
        country = aCountry;
        governingDistrict = aGoverningDistrict;
        postalArea = aPostalArea;
    }
    
    return self;
}

@end
