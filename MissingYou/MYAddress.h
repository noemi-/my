//
//  MYAddress.h
//  MissingYou
//
//  Created by Noemi Quezada on 11/8/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MYAddress : NSObject

@property (strong, nonatomic)NSString * addressId;
@property (strong, nonatomic)NSString * addressLine;



/* Init method */
-(id)initWithAddressId: (NSString *) anAddressId anAddressLine: (NSString *)anAddressLine;

@end
