//
//  MYDetailViewController.h
//  MissingYou
//
//  Created by Design on 11/25/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface MYDetailViewController:UIViewController <UIScrollViewDelegate>
{
    IBOutlet UIScrollView * scroller;
}
@property (weak, nonatomic) IBOutlet PFImageView *profileImageView;
@property (weak, nonatomic) PFObject *personsDetails;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *ageLabel;
@property (weak, nonatomic) IBOutlet UILabel *genderLabel;
@property (weak, nonatomic) IBOutlet UILabel *foundLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactPhoneNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactEmailLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactAddressLabel;

@end
