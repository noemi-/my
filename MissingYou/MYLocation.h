//
//  MYLocation.h
//  MissingYou
//
//  Created by Noemi Quezada on 11/8/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MYLocation : NSObject

@property (strong, nonatomic)NSString * locationId;
@property (strong, nonatomic)NSString * country;
@property (strong, nonatomic)NSString * governingDistrict;
@property (strong, nonatomic)NSString * postalArea;
@property (strong, nonatomic)NSString * cityTown;

-(id)initWithLocationId: (NSString * )aLocationId aCountry: (NSString *)aCountry aGoverningDistrict: (NSString *)aGoverningDistrict aPostalArea: (NSString *)aPostalArea aCityTown: (NSString *)aCityTown; 

@end
