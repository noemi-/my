//
//  MYPost.m
//  MissingYou
//
//  Created by Noemi Quezada on 11/8/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import "MYPost.h"


@implementation MYPost

@synthesize postId;
@synthesize item;
@synthesize contactPerson;
@synthesize location; 

-(id)initWithPostId:(NSString *)aPostId anItem:(id<MYPostItemProtocol,NSObject>)anItem aLocation:(MYLocation *)aLocation aContactPerson:(MYContactPerson *)aContactPerson
{
    self = [super init];
    if (self)
    {
        item = anItem;
        postId = aPostId;
        location = aLocation;
        contactPerson = aContactPerson;
    }
    return self;
}

@end
