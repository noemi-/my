//
//  MYDetailViewController.m
//  MissingYou
//
//  Created by Design on 11/25/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import "MYDetailViewController.h"

@interface MYDetailViewController ()

@property(strong, nonatomic)id addressId;

@end

@implementation MYDetailViewController
@synthesize profileImageView;

@synthesize personsDetails;
@synthesize nameLabel;
@synthesize ageLabel;
@synthesize genderLabel;
@synthesize foundLabel;
@synthesize locationLabel;
@synthesize contactNameLabel;
@synthesize contactAddressLabel;
@synthesize contactEmailLabel;
@synthesize contactPhoneNumberLabel;
@synthesize addressId;



- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setLabels];
    scroller.delegate = self;
    [scroller setScrollEnabled:YES];
  //  scroller.contentSize = self.view.frame.size;
    [scroller setContentSize:CGSizeMake(320, 700)];
    // Do any additional setup after loading the view.
}
-(void)setLabels
{
    PFObject * person = personsDetails[@"person"];
    
    [person fetchIfNeededInBackgroundWithBlock:^(PFObject *person, NSError *error) {
        // Configure the cell
        
        PFFile *profileImage = [person objectForKey:@"personPicture"];
        profileImageView.image = [UIImage imageNamed:@"temp_avatar.jpg"];
        profileImageView.file = profileImage;
        [profileImageView loadInBackground];
        
        nameLabel.text = [person objectForKey:@"name"];
        
        NSNumber * theAge = [person objectForKey:@"age"];
        
        ageLabel.text = [NSString stringWithFormat:@"Age: %@", [theAge stringValue]];
        
        genderLabel.text = [NSString stringWithFormat:@"Gender: %@",[person objectForKey:@"gender"]];
        
        BOOL foundValue = [[person objectForKey:@"found"]boolValue];
        //0 is FALSE, 1 is TRUE
        if(foundValue == 1) //if found
        {
            foundLabel.text = [NSString stringWithFormat:@"Found"];
        }
        else{
            foundLabel.text = [NSString stringWithFormat:@"Not Found"];
        }
    
    }];
    
    PFObject *location = personsDetails[@"location"];
    [location fetchIfNeededInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        locationLabel.text = object[@"location"];
    }];
    
    PFObject *contact = personsDetails[@"contactPerson"];
    [contact fetchIfNeededInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        contactNameLabel.text = object[@"name"];
        contactEmailLabel.text = object[@"email"];
        contactPhoneNumberLabel.text = object[@"phoneNumber"];

    }];
    



    


}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
