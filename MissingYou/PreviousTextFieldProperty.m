/* Name: Noemi Quezada
 * Email: noemi01@csu.fullerton.edu
 * Description: This file is reponsible for implementing the previousTextfield attribute that is being added to UITextField
 */


#import "PreviousTextFieldProperty.h"
#import <objc/runtime.h>

static Byte defaultHashKey;

@implementation UITextField (PreviousTextFieldProperty)

-(UITextField*) previousTextField
{
    return objc_getAssociatedObject( self, &defaultHashKey );
}

-(void) setPreviousTextField: (UITextField *)previousTextField
{
    objc_setAssociatedObject( self, &defaultHashKey, previousTextField,
                             OBJC_ASSOCIATION_RETAIN_NONATOMIC );
}

@end
