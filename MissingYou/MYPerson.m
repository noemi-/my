//
//  MYPerson.m
//  MissingYou
//
//  Created by Noemi Quezada on 11/6/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import "MYPerson.h"

@implementation MYPerson

@synthesize objectId; 
@synthesize firstName;
@synthesize lastName;
@synthesize middleName;
@synthesize hairColor;
@synthesize eyeColor;
@synthesize height;
@synthesize weight;
@synthesize gender; 
@synthesize personPicture;
@synthesize found;
@synthesize age;

-(id)initWithPersonId:(NSString *)aPersonId aFirstName:(NSString *)afirstName aLastName:(NSString *)aLastName aMiddleName:(NSString *)aMiddleName aHairColor:(NSString *)aHairColor anEyeColor:(NSString *)anEyeColor aHeight:(double)aHeight aWeight:(double)aWeight aGender:(NSString *)aGender anAge: (double)anAge aProfilePicture:(PFFile *)aPicture
{
    self = [super init];
    if (self)
    {
        objectId = aPersonId;
        
        firstName = afirstName;
        middleName = aMiddleName;
        lastName = aLastName;
        hairColor = aHairColor;
        eyeColor = anEyeColor;
        height = aHeight;
        weight = aWeight;
        gender = aGender;
        personPicture = aPicture;
        age = anAge;
        found = false;
        
    }
    
    return self; 
}

@end
