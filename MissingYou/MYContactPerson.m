//
//  MYContactPerson.m
//  MissingYou
//
//  Created by Noemi Quezada on 11/8/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import "MYContactPerson.h"

@implementation MYContactPerson

@synthesize contactPersonId;
@synthesize email;
@synthesize name;
@synthesize phoneNumber;
@synthesize address; /* Confused on this one */

-(id)initWithContactPersonId:(NSString *)aContactPersonId aName:(NSString *)aName aPhoneNumber:(NSString *)aPhoneNumber anEmail:(NSString *)anEmail anAddress:(MYAddress *)anAddress
{
    self = [super init];
    if (self)
    {
        contactPersonId = aContactPersonId;
        email = anEmail;
        name = aName;
        phoneNumber = aPhoneNumber;
        address = anAddress;
    }
    return self; 
}

@end
