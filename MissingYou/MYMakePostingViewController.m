//
//  MYMakePostingViewController.m
//  MissingYou
//
//  Created by Maira on 11/9/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import "MYMakePostingViewController.h"
#import "SWRevealViewController.h"
#import <Parse/Parse.h>
#import "PreviousTextFieldProperty.h"
#import "NextTextFieldProperty.h"

@interface MYMakePostingViewController ()

@end

@implementation MYMakePostingViewController
@synthesize menuButton;
@synthesize profileImageView;
@synthesize personAgeTextField;
@synthesize personNameTextField;
@synthesize contactNameTextField;
@synthesize addressTextField;
@synthesize emailTextField;
@synthesize phoneNumberTextField;
@synthesize genderSwitch;
@synthesize locationFoundTextField;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor grayColor] forKey:NSForegroundColorAttributeName];
    
    /* Setting Text Fields Next */
    personNameTextField.nextTextField = personAgeTextField;
    personAgeTextField.nextTextField = locationFoundTextField;
    locationFoundTextField.nextTextField = contactNameTextField;
    contactNameTextField.nextTextField = phoneNumberTextField;
    phoneNumberTextField.nextTextField = addressTextField;
    addressTextField.nextTextField = emailTextField;
    emailTextField.nextTextField = personAgeTextField;
    
    /* Setting Text Fields Previous */
    personNameTextField.previousTextField = emailTextField;
    personAgeTextField.previousTextField = personNameTextField;
    locationFoundTextField.previousTextField = personAgeTextField;
    contactNameTextField.previousTextField = locationFoundTextField;
    phoneNumberTextField.previousTextField = contactNameTextField;
    addressTextField.previousTextField = phoneNumberTextField;
    emailTextField.nextTextField = addressTextField;
    
    /* Create a tap gesture recognizer and assign it to the scrollable view */
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self registerForKeyboardNotifications];
    [self.view addGestureRecognizer:tapGesture];

    
    // Do any additional setup after loading the view.
    [profileImageView setImage: [UIImage imageNamed:@"temp_avatar.jpg"]];
   // [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"kite background-01.jpg"]]];
   // [self.view setBackgroundColor:[UIColor whiteColor]];
    [self customSetup];

}

-(void)dismissKeyboard
{
    /* Check which textField is the first Responder and resign the FirstResponder */
    if(![self isFirstResponder])
    {
        if ([personNameTextField isFirstResponder])
        {
            [personNameTextField resignFirstResponder];
        }
        if ([personAgeTextField isFirstResponder])
        {
            [personAgeTextField resignFirstResponder];
        }
        if ([contactNameTextField isFirstResponder])
        {
            [contactNameTextField resignFirstResponder];
        }
        if ([phoneNumberTextField resignFirstResponder])
        {
            [phoneNumberTextField resignFirstResponder];
        }
        if ([addressTextField isFirstResponder])
        {
            [addressTextField resignFirstResponder];
        }
        if ([emailTextField isFirstResponder])
        {
            [emailTextField resignFirstResponder];
        }
        if ([locationFoundTextField isFirstResponder])
        {
            [locationFoundTextField resignFirstResponder];
        }
        
    }
}
- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    menuButton.tintColor = [UIColor blackColor];
    if ( revealViewController )
    {
        menuButton.target = self.revealViewController;
        menuButton.action = @selector(revealToggle:);
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)picturePressed:(id)sender
{
    //show the app menu
    [[[UIActionSheet alloc] initWithTitle:nil
                                 delegate:self
                        cancelButtonTitle:@"Close"
                   destructiveButtonTitle:nil
                        otherButtonTitles:@"Take photo", @"Upload", nil]
     showInView:self.view];
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0:
            [self takePicture];
            break;
        case 1:
            [self uploadPicture];
            break;
            
    }
}
-(void)takePicture
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
#if TARGET_IPHONE_SIMULATOR
    //if its a simulator then open the photolibrary
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
#else
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
#endif
    imagePickerController.editing = YES; //allows user to edit the photo before submitting
    imagePickerController.delegate = (id)self;
    [self presentViewController:imagePickerController animated:YES completion:NULL];
    
}
-(void)uploadPicture
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    
    //if its a simulator then open the photolibrary
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    imagePickerController.delegate = (id)self;
    [self presentViewController:imagePickerController animated:YES completion:NULL];
    
}

#pragma mark - Image picker delegate methdos
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
	chosenPicture = [info objectForKey:UIImagePickerControllerOriginalImage];
    // Resize the image from the camera

    profileImageView.image=chosenPicture;
    
    [[picker presentingViewController] dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [[picker presentingViewController] dismissViewControllerAnimated:YES completion:nil];
}




#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    
    PFObject * post = [PFObject objectWithClassName:@"Post"];
    PFObject * person = [PFObject objectWithClassName:@"Person"];
    PFObject * contactPerson = [PFObject objectWithClassName:@"ContactPerson"];
    PFObject * addresss = [PFObject objectWithClassName:@"Address"];
    PFObject * location = [PFObject objectWithClassName:@"Location"];
    
    // Set Picture
    NSData *imageData = UIImageJPEGRepresentation(profileImageView.image, 0.8);
    NSString *filename = [NSString stringWithFormat:@"%@.png", personAgeTextField.text];
    PFFile *imageFile = [PFFile fileWithName:filename data:imageData];
    [person setObject:imageFile forKey:@"personPicture"];
    
    person[@"name"] = personNameTextField.text;
    person[@"age"] = [NSNumber numberWithInteger:[personAgeTextField.text integerValue]];
    if (genderSwitch.selectedSegmentIndex == 0)
    {
        person[@"gender"] = @"female";
    }
    else{
        person[@"gender"] = @"male";
    }
    
    [person saveInBackground];
    
    contactPerson[@"name"] = contactNameTextField.text;
    contactPerson[@"phoneNumber"] = phoneNumberTextField.text;
    contactPerson[@"email"] = phoneNumberTextField.text;
    addresss[@"addressLine"] = addressTextField.text;
    
    [addresss saveInBackground];
    contactPerson[@"contactAddress"] = addresss;
    
    [contactPerson saveInBackground];
    
    location[@"location"] = locationFoundTextField.text;
    
    [location saveInBackground];
    
    post[@"person"] = person;
    post[@"contactPerson"] = contactPerson;
    post[@"location"] = location;
    
    
    [post saveInBackground];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    NSLog(@"touchesBegan:withEvent:");
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}

/* REFERENCE: https://developer.apple.com/library/ios/documentation/StringsTextFonts/Conceptual/TextAndWebiPhoneOS/KeyboardManagement/KeyboardManagement.html  */

// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    UIScrollView * tempScroll = (UIScrollView *) self.view;
    NSDictionary* info = [aNotification userInfo];
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
    tempScroll.contentInset = contentInsets;
    tempScroll.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    CGRect aRect = self.view.frame;
    aRect.size.height -= keyboardSize.height;
    if (!CGRectContainsPoint(aRect, CGPointMake(self.activeTextField.frame.origin.x, (self.activeTextField.frame.origin.y+ 20)) ))
    {
        CGPoint scrollPoint = CGPointMake(0.0, self.activeTextField.frame.origin.y - ((keyboardSize.height)));
        [tempScroll setContentOffset:scrollPoint animated:YES];
    }
    /* If the text field doesn't hide the keyboard scroll the view to its default position */
    else
    {
        CGPoint scrollPoint = CGPointMake(0.0, self.activeTextField.frame.origin.y + (aRect.origin.y - self.activeTextField.frame.origin.y));
        [tempScroll setContentOffset:scrollPoint animated:YES];
    }
}

- (BOOL)textFieldShouldBeginEditing: (UITextField *) textField
{
    
    /* Set the textfield being edited as active textfield */
    self.activeTextField = textField;
    
    /* Create Keyboard Toolbar with Previous, Next and Done Button */
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    [toolbar sizeToFit];
    
    UIBarButtonItem *prevButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @"Previous"
                                   style: UIBarButtonItemStyleDone
                                   target: self
                                   action:@selector(previousClicked:)];
    
    UIBarButtonItem *nextButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @"Next"
                                   style: UIBarButtonItemStyleDone
                                   target: self
                                   action:@selector(nextClicked:)];
    
    UIBarButtonItem *flexButton = [[UIBarButtonItem alloc]
                                   initWithBarButtonSystemItem: UIBarButtonSystemItemFlexibleSpace
                                   target: self
                                   action: nil];
    
    UIBarButtonItem *doneButton =[[UIBarButtonItem alloc]
                                  initWithBarButtonSystemItem: UIBarButtonSystemItemDone
                                  target: self
                                  action: @selector(doneClicked:)];
    
    NSArray* itemsArray = @[prevButton, nextButton, flexButton, doneButton];
    
    [toolbar setItems: itemsArray];
    
    [textField setInputAccessoryView: toolbar];
    
    return YES;
}


// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIScrollView * tempScroll = (UIScrollView *) self.view;
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    tempScroll.contentInset = contentInsets;
    tempScroll.scrollIndicatorInsets = contentInsets;
    
    
}

/* Function called when Next Button on the Keyboard Toolbar is pressed, will move to next textfield */
-(void)nextClicked: (UIBarButtonItem * )sender
{
    UITextField* next = self.activeTextField.nextTextField;
    if (next) {
        [next becomeFirstResponder];
    }
}

/* Function called when Previous Button on the Keyboard Toolbar is pressed, will move the previous textfield */
-(void)previousClicked: (UIBarButtonItem * )sender
{
    UITextField* previous = self.activeTextField.previousTextField;
    if (previous) {
        [previous becomeFirstResponder];
    }
}

/* Function called when Done Button on the Keyboard Toolbar is pressed */
- (void) doneClicked: (UIBarButtonItem*) sender
{
    /* Resign the keybarod */
    [self.activeTextField resignFirstResponder];
    
}

@end
