//
//  MYMenuTableViewController.h
//  MissingYou
//
//  Created by Maira on 11/8/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface MenuTableViewCell : UITableViewCell
@property (nonatomic) IBOutlet UILabel *label;
@end


@interface MYMenuTableViewController : UITableViewController


@property (nonatomic, strong) NSArray *menuoptions;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;

@end

