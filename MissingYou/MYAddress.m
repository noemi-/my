//
//  MYAddress.m
//  MissingYou
//
//  Created by Noemi Quezada on 11/8/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import "MYAddress.h"

@implementation MYAddress

@synthesize addressId;
@synthesize addressLine;


-(id)initWithAddressId:(NSString *)anAddressId anAddressLine:(NSString *)anAddressLine
{
    self = [super init];
    if (self)
    {
        addressId = anAddressId;
        addressLine = anAddressLine;

    }
    
    return self; 
}

@end
