//
//  MYPost.h
//  MissingYou
//
//  Created by Noemi Quezada on 11/8/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MYContactPerson.h"
#import "MYPerson.h"
#import "MYLocation.h"
#import "MYPostItemProtocol.h"

@interface MYPost : NSObject


@property(strong, nonatomic)NSString * postId;
@property(assign, nonatomic)id<MYPostItemProtocol, NSObject> item;
@property(strong, nonatomic)MYLocation * location;
@property(strong, nonatomic)MYContactPerson * contactPerson;

-(id)initWithPostId:(NSString *)postId anItem: (id<MYPostItemProtocol, NSObject>)anItem aLocation: (MYLocation* ) aLocation aContactPerson: (MYContactPerson *)aContactPerson;

@end
