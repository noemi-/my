//
//  MYPostItem.h
//  MissingYou
//
//  Created by Noemi Quezada on 11/8/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MYPostItemProtocol <NSObject>

@property (strong, nonatomic)NSString * objectId;


@end
