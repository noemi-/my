//
//  main.m
//  MissingYou
//
//  Created by Noemi Quezada on 11/6/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MYAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MYAppDelegate class]));
    }
}
