/* Name: Noemi Quezada
 * Email: noemi01@csu.fullerton.edu
 * Description: This file is reponsible for implementing the nextTextfield attribute that is being added to UITextField
 */

#import "NextTextFieldProperty.h"
#import <objc/runtime.h>

static Byte defaultHashKey;

@implementation UITextField (NextTextFieldProperty)

-(UITextField*) nextTextField
{
    return objc_getAssociatedObject( self, &defaultHashKey );
}

-(void) setNextTextField: (UITextField *)nextTextField
{
    objc_setAssociatedObject( self, &defaultHashKey, nextTextField,
                             OBJC_ASSOCIATION_RETAIN_NONATOMIC );
}

@end
